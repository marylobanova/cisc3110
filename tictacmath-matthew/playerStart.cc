/*The playerStart function state who's turn it is in the game*/
#include<iostream>

int playerStart(int num){
  /*The player takes the value in num and checks to see if it has a remainger of zero or
    not and uses that value to determine who's turn it is.*/
  if(num%2!=0){                                   
    std::cout<<"Player 1 it's your turn"<<std::endl;
    std::cout<<"_______________________"<<std::endl;
  }
  
  else
    std::cout<<"Player 2 it's your turn!"<<std::endl;
    std::cout<<"________________________"<<std::endl;
}
