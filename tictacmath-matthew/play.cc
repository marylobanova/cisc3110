#include <iostream>
#include "validate.h"

//The play function allows for the player to enter his or
//values for the number and location in the array
int play(int array[][3]){
 int num;
 int row;
 int col;
 std::cout<<" "<<std::endl;
 std::cout<<"Please choose a number between 1 to 9."<<std::endl;
 std::cin>>num;
 std::cout<<"Please choose the row you will like your number "
          <<"to be placed."<<std::endl;
 std::cin>>row;
 std::cout<<"Please choose the column you will like your number "
          <<"to be placed."<<std::endl;
 std::cin>>col;

 validate(array,num,row,col);//Once entered the function validate is called
 //and evaluates if these values are valid to be passed to the board
 

}
