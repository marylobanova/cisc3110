/*The display function displays the current state of the board*/
#include<iostream>
#include "checkForwin.h"

using namespace std;

void display(int board[][3]){
  bool win;
  std::cout<<"      |      |     "<<std::endl;
  std::cout<<"  "<<board[0][0]<< "   |  " <<board[0][1] << "   |  "
           << board[0][2] << "   " <<std::endl;
  std::cout<<"______|______|______" <<std::endl;
  std::cout<<"      |      |      " <<std::endl;
  std::cout<<"  "<<board[1][0]<< "   |  " <<board[1][1] << "   |  "
	   << board[1][2] << "   " <<std::endl;
  std::cout<<"______|______|______"<<std::endl;
  std::cout<<"      |      |      "<<std::endl;
  std::cout<<"  "<<board[2][0]<< "   |  " <<board[2][1] << "   |  "
           << board[2][2] << "   " <<std::endl;
  std::cout<<"      |      |      "<<std::endl;
 /*The checkForwin function is called and stored in the win variable as true or
   false. If true is returned the if statement access the cout function*/
 win=checkForwin(board);
  if(win==true)
    std::cout<<"YOU WIN, THE GAME IS OVER"<<std::endl;
  
return;
}
