#include<iostream>

#include "playerStart.h"
#include "play.h"

#include "display.h"
#include "checkForwin.h"

using namespace std;

int main(){
int array[3][3]={0};//Initialize array to zero
  int count=0;
  bool check;

/*The while loop is true as long as no player as won or
  all locations have not been occupied in the array */
while(true){
   count++;//helps keep track of current value of the loop
   std::cout<<"   "<<std::endl;
   
   std::cout<<"Current state of the board!"<<std::endl;
   std::cout<<"___________________________"<<std::endl;
   
   std::cout<<" "<<std::endl;
   display(array);
    
   std::cout<<" "<< std::endl;
   playerStart(count);//suggest whos turn it is
  
   std::cout<<" "<<std::endl;
   play(array);//Allows the player to enter number and location.
  
   std::cout<<" "<<std::endl;
   display(array);//displays board after player has entered a valid number and location
  
   std::cout<<" "<<std::endl;
   check=checkForwin(array);   /*****************************************************/ 
   if(check==true)             /*Calls the checkForwin function and returns true or */
     break;                    /*false to the variable check and if true breaks the */
                               /*loop letting the players know who has won          */
                               /*****************************************************/

   if(count > 9)//Since the board only has 9 locations the
     break;    //the while loop will break after the 9th turn.
 }
   return 0;
}
