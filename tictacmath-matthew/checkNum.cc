/*The checkNum function is a bool function that checks to see if the number the 
  player has  enterd has been used already*/

bool checkNum(int array[][3], int num){
  int keep;
  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){    /**************************************************/
      keep=array[i][j];        /*For loop function stores the value in location  */ 
      if(keep==num){           /*into the variable keep and checks to see if keep*/ 
	  return false;        /*is equivalent to the number the player entered  */
      }                        /*if it does the function returns false           */
      else                     /**************************************************/
	return true;
    }
  }
}
