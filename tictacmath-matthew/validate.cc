#include <iostream>

/*The validate function processes if the values entered by the player 
 is valid are not, if they are valid then they are entered into the 
 array to be displayed otherwise the user is prompt to try again!*/
#include "fullValidation.h"
#include "play.h"

#include "checkNum.h"

int validate(int array[][3], int num, int row, int col){
  bool check;
  check=checkNum(array,num);
  /*if statement below checks to see if number already exist in the array*/
  if(check==false){
 
    num=array[row][col];//value of num is the number already stored in location
    fullValidation(array,num,row,col);
    // std::cout<<"Please try again invalid information entered."<<std::endl;
    play(array);
  }
  /*else if statement below to see if there is invalid numbers entered for
    value that the player wants to enter and location where player wants it
    to be displayed*/
 else if(((num<0)||(num>9))||(((row||col)<0)||((row||col)>2))){
    num = 0;//num will be stored as zero
    fullValidation(array,num,row,col);
    //std::cout<<"Please play again invalid information entered."<<std::endl;
    play(array);
  }
/*else if statement below checks to see if a number is already located in 
  the location the player wants to use to store their number*/
else if(array[row][col]!=0){
  for(int i=0; i<3; i++){
    for(int j =0; j<3; j++){
      if((i==row)&&(j==col)){
	  num = array[row][col];
      }
    }
  }
      fullValidation(array,num,row,col);
      // std::cout<<"Please try again invalid information entered."<<std::endl;
      play(array);
}
  /*else statement allows for array to store the value numb if
    all information is valid*/
 else
    array[row][col]=num;
 
}



