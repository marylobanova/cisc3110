//The CheckForwin function is a bool statement that returns true by finding if the sum of three
//consecutive elements adds up to 15.

bool checkForwin(int array[][3]){
  int sum0=0;        //There are 8 possibilities for the player to win
  int sum1=0;        //so to the left is the initialization of each sum
  int sum2=0;        //to zero
  int sum3=0;
  int sum4=0;
  int sum5=0;
  int sum6=0;
  int sum7=0;

  for(int i=0;i<3;i++){
    sum0+=array[0][i];   //for loop to the left checks and adds elements
    sum1+=array[i][0];   //as they are being entered by the player
    sum2+=array[1][i];
    sum3+=array[i][1];
    sum4+=array[2][i];
    sum5+=array[i][2];
    sum6+=array[i][2-i];
    sum7+=array[i][i];
  }

  if(sum0==15)  //If statements return true if one of the sums return 15.
    return 1;
  if(sum1==15)
    return 1;
  if(sum2==15)
    return 1;
  if(sum3==15)
    return 1;
  if(sum4==15)
    return 1;
  if(sum5==15)
    return 1;
  if(sum6==15)
    return 1;
  if(sum7==15)
    return 1;
  else
    return 0;
}
