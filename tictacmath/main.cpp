//Maria Lobanova
//Tic Tac Math

#include <iomanip>
#include <iostream>
#include <string>
#include "interface.h"

// This program was designed according to the MVC principles
// Class 'Game' isolates the operational part of the game (Controller) 
// and connects 'Interface' class (View) with game attributes and logic details (Model)
//
// Remaining classes have no dependencies and require minimum changes
// in case when logic of the game has to be modified.

int main() {

	Interface interface = Interface();
	interface.startNewGame();
	return 0;

}



