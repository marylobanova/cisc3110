//Maria Lobanova
//Tic Tac Math

#include "interface.h"

//default constructor
Interface::Interface(){}

void Interface::startNewGame()
{
	int num;
	pair <int,int> position;
	Player player;
	string 	response;

	//ask if user wants to start another game 
	//if the previous game is over
	do {
		cout << "Do you want to start a new game? (y/n) ";
		getline (cin,response);

		//validate for correct response
		while (response != "y" &&  response != "Y" && response != "n" && response != "N") {
			cout << "This is invalid answer. Please enter y (for yes) or n (for no): ";
			getline (cin, response);
			}

		//start new game
		if (response == "y" || response == "Y") {
			game = Game();
			displayGameRules();
			setPlayer();
			
			//continue the game until there are no turns left
			//of if there is a winner
			while (game.getTurnsLeft() > 0) {
				game.displayBoard();
				player = game.getCurrentPlayer();
				cout << endl << player.getName() << ", please make your move." << endl;
				position = getPosition();
				num = getPlayNum();

				while (!game.addPlayedNum(num)){
					cout << "This number is already on board. ";
					num = getPlayNum(); 
				}
				cout << "\nYour move has been made." << endl;
				game.playMove(position.first, position.second, num);
				
				//exit current game if player won
				if (game.evaluateGameState() == 1) {
					cout << "\nCongratulations! You won!" << endl;
					game.printGameStatistics();
				}
			}	

			//if there is no winner and no more turns left
			if (game.evaluateGameState() == 0) {
				cout << "Game is over. There is no winner." << endl;
				game.printGameStatistics();
			}
		}
		//exit the game if user has chosen not to continue
		else {
			cout << "\nGoodbye!\n" << endl;
			exit(0);
		}
	} while (response == "y" || response == "Y");
	
	exit(0);
}


void Interface::setPlayer()
{	
	int numPlayers;
	string 	name,
					sNumPlayers;

	cout << "\nHow many players will be in the game? ";
	cin >> sNumPlayers;

	//check if an input is a digit
	while (!isNumber(sNumPlayers)) {
		cout << "This entry is invalid. Please enter a number: ";
		cin >> sNumPlayers;
	}
	//convert an input to an integer	
	numPlayers = stoi(sNumPlayers, nullptr, 10);
	//there should be at least two players
	while (numPlayers < 2) {
		cout << "Number of players can not be less then two. Enter number of players: ";
		cin >> numPlayers; 
	}
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //ignore new line char
	//get names of players and store them 
	for (int i = 0; i < numPlayers; i++) {
		cout << "Enter " << i+1 << " player's name: ";
		getline (cin, name);
		//player name has to be unique
		while (!game.addPlayer(Player(name))) {
			cout << "This name has already been taken. Choose different name: ";
			getline (cin, name);
		}
	}
}


void Interface::displayGameRules()
{
	cout << "\nWecome to the Tac-Math game!" << endl;
	cout << "\n###GAME RULES###" << endl;
	cout << "You will be offered to select a play number from ";
	cout << "1 to " << game.getTurnsLeft() << " and a position on the board " << endl;
	cout << "(you will need to specify the column and the row numbers). " << endl;
	cout << "Please note that you are not able to chose a play number or " << endl;
	cout << "the position that was already taken by the other player. " << endl;
	cout << "The winner is the player who will first complete a row, a column " << endl;
	cout << "or a diagonal with numbers that sum up to " << game.getWinSum() << "." << endl;
	cout << "Good luck!" << endl;
}


pair<int,int> Interface::getPosition()
{ 
	int row,
			column;

	string	sRow,
					sColumn;
	
	do {
		cout << "Choose position row: ";
		cin >> sRow; 
		//check if an input is a digit
		while (!isNumber(sRow)) {
			cout << "\nThis entry is invalid. Please enter a number: ";
			cin >> sRow;
		}	
		row = stoi(sRow, nullptr, 10); //convert an input to an integer
		cout << "Choose position column: ";
		cin >> sColumn; 
		//check if an input is a digit
		while (!isNumber(sColumn)) {
			cout << "\nThis entry is invalid. Please enter a number: ";
			cin >> sColumn;
		}
		column = stoi(sColumn, nullptr, 10);		//convert an input to an integer
		if (!game.isMovePossible(row, column)) 	//check if position is valid and available
			cout << "\nThis position is either invalid or it has been taken. Try again." << endl;	
	}	while (!game.isMovePossible(row, column));
	
	return make_pair(row, column);
}


int Interface::getPlayNum()
{
	int num;
	string sNum;	
	//keep asking to choose a number
	//if the number is not valid
	do {
		cout << "Choose the play number: ";
		cin >> sNum; 
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //ignore new line char
		//check if an input is a digit
		while (!isNumber(sNum)) {
			cout << "This entry is invalid. Please enter a number: ";
			cin >> sNum;
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //ignore new line char
		}	
		num = stoi(sNum, nullptr, 10); //convert an input to an integer
		
		if (!game.isNumValid(num))
			cout << "This number is not in the range of available numbers. Try again." << endl;
	} while (!game.isNumValid(num));	
	
	return num;
}
