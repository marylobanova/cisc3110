//Maria Lobanova
//Tic Tac Math

#include "game.h"

//default constructor
Game::Game()
{
	currentPlayerIdx = 0;
	board = Board();
	turnsLeft = board.getSize()*board.getSize();
}


void Game::printGameStatistics()
{
	displayBoard();
	cout << "\nGAME STATISTICS:" << endl;
	cout << "---------------------" << endl;
	cout << setw(15) << left << "Total moves: " << playedNums.size() << endl;
	cout << "Total players: " << players.size() << endl;
	for (int i=0; i < players.size(); i++)
		cout << "Player " << i+1 << ": " << players[i].getName() << endl;
	if (evaluateGameState() == 1)
		cout << left << "Winner: " << players[currentPlayerIdx].getName() << endl;
	cout << "---------------------" << endl;
}


//store played numbers
bool Game::addPlayedNum(int num)
{
	for (int i = 0; i < playedNums.size(); i++){
		if (num == playedNums[i])
			return false;
	}
	playedNums.push_back(num);
	return true;
}


//store all players
bool Game::addPlayer(Player player)
{
	for (int i=0; i < players.size(); i++){ 
		if (player.getName() == players[i].getName())
			return false;
	}
	players.push_back(player);
	return true;
}

//1 - win
//0 - draw
//additional states can be added
short int Game::evaluateGameState()
  {
    for(int i = 0; i < board.getSize(); ++i) {
      if (board.rowSum(i) == board.getWinSum() || 
					board.columnSum(i) == board.getWinSum() || 
					board.lDiagonalSum() == board.getWinSum() ||     
					board.rDiagonalSum() == board.getWinSum())
        return 1;
    }
    return 0;
  }


void Game::playMove(int row, int column, int num)
{
	addPlayedNum(num);
	board.setValueToBoard(row, column, num);
	turnsLeft--;
	if (!evaluateGameState())
		switchPlayer(); //switch player if there is no winer
}


Player Game::getCurrentPlayer()
{
	return players[currentPlayerIdx];
}


void Game::switchPlayer()
{
	currentPlayerIdx = (currentPlayerIdx+1)%players.size();
}


int Game::getTurnsLeft()
{
	return turnsLeft;
}


void Game::displayBoard()
{
	board.display();
}


bool Game::isNumValid (int num)
{
	return (0 < num && num <= board.getSize()*board.getSize());
}


bool Game::isMovePossible(int row, int column)
{
	return board.isPositionValid(row,column) && board.isPositionAvailable(row,column);
}


int Game::getWinSum()
{
	return board.getWinSum();
}
