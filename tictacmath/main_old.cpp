//Maria Lobanova
//Tic Tac Math

#include <iomanip>
#include <iostream>
#include <string>
#include "board.h"
#include "validator.h"

string playNumber,
			 playRow,
			 playColumn;

int number,
		row,
		column,
		turn;

int main ()
{
	turn = 1;
	
	//check if there are still avalable plays
	while(turn <= numberOfTurns()) {	
		
		//print current player number
		cout << "Player " << turn%2 << endl;
		
		//get play number
		cout << "Please choose your play number: ";
			
		//get play number
		//repeat prompt until the validation
		//for type, range and duplication is passed
		while(true) {
			cin >> playNumber;
			//check if input is a number
			if (isNumber(playNumber)) {
				//convert an input from string to integer
				number = stoi(playNumber, nullptr, 10);
				//check if input is within the exitsting range
				//and if this number was not taken already
				if(isInRange(number, 9) && !isNumberOnBoard(number)) 
					break;
				else {
				cout << "Number that you enterd is either already on the board or out of range. " << endl;
				cout << "Please choose another number: ";
				}
			}
			else 
				cout << "Play number has to be an integer. Please choose a play number: ";
		}

		
		//get board position
		//repeat prompt until the validation 
		//for existing position is passed
		while(true) {
			//get row  position
			//repeat prompt until the validation 
			//for range and type is passed
			cout << "Please choose the position row: ";
			while(true) {
				cin >> playRow;
				//check if input is a number
				if (isNumber(playRow)) {
					//convert an input from string to integer
					row = stoi(playRow, nullptr, 10);
					//check if input is in the existing range
					if(isInRange(row, boardSize()-1)) 
						break;
					else
						cout << "This row number does not exist! Please choose existing row number: ";
				}
				else
					cout << "Row number has to be an integer. Please choose a row number: ";
			}
			
			//get position column
			//repeat prompt until the validation 
			//for range and type is passed
			cout << "Please choose the position column: ";
			while(true) {
				cin >> playColumn;
				//check if input is a number
				if (isNumber(playColumn)) {
					//convert an input from string to integer
					column = stoi(playColumn, nullptr, 10);
					//check if input is in the existing range
					if(isInRange(column, boardSize()-1)) 
						break;
					else 
						cout << "This column number does not exist! Please choose existing column number: ";
					
				}
				else
					cout << "Column number has to be an integer. Please choose a column number: ";
			}
		
		//check if chosen position is available
		//if position is not available - get the new position
		if (isPositionAvailable(row, column)) 
			break;
		cout << "This position has been already taken. Please choose a different position!" << endl;
		}	

		//write value to the board
		//and display current board state
		setValueToBoard(row, column, number);
		display();

		//check if current player won and either exit the game
		//or continue to the next draw
		if(evaluateGameState()) {
			cout << "Player " << turn%2 << " won!" << endl;
			break;
		}
		
		//increment the turn to switch the player
		turn++;
	}

	return 0;	
}



