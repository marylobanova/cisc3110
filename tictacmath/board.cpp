//Maria Lobanova
//Tic Tac Math

#include "board.h"


Board::Board()
{
	size = 3;
	winSum = 15;
	board = new int*[size];
	for(int i=0; i < size; ++i) {
		board[i] = new int[size];					//allocate memory for second dimension
		fill(board[i], board[i]+size, 0); //initialize all values to zero
	}
}


Board::Board(int size, int winSum)
{
	this->size = size;
	this->winSum = winSum;
	board = new int*[size];
	for(int i=0; i < size; ++i) {
		board[i] = new int[size];					//allocate memory for second dimension
		fill(board[i], board[i]+size, 0); //initialize all values to zero
	}
}


void Board::setValueToBoard(int row, int column, int num)
{
	board[row][column]=num;
}


//check if position wasn't taken already
bool Board::isPositionAvailable(int row, int column) 
{
	return !board[row][column];
}


//check if row and column are within the board size
bool Board::isPositionValid (int row, int column)
{
	return (0 <= row && row < size) && (0 <= column && column < size);
}


//calculate sum for left top to right bottom diagonal
int Board::lDiagonalSum()
{ 
	int sum = 0;
	for (int i = 0; i < size ; ++i) {
		if (board[i][i] == 0)
			return 0;
		else
			sum += board[i][i];
	}
	return sum;
}


//calculate sum for right top to left bottom diagonal
int Board::rDiagonalSum()
{
	int sum = 0;
	for (int i = 0; i < size ; ++i) {
		if (board[i][size-i-1] == 0)
			return 0;
		else
			sum += board[i][size-i-1];
	}
		return sum;
}

			
//calculate sum for each row and check if all cells are filled with values
int Board::rowSum(int row)
{
	int sum = 0;
	for (int i = 0; i < size; i++) {
		if (board[row][i] == 0)
			return 0;
		else
			sum += board[row][i];
	}
			return sum;
}


//calculate sum for each column and check if all cells are filled with values
int Board::columnSum(int column)
{
	int sum = 0;
	for (int i = 0; i < size; i++) {
		if (board[i][column] == 0)
			return 0;
		else
			sum += board[i][column];
	}
		return sum;
}


//print out the board
void Board::display()
{
	cout << "\n  ##PLAY BOARD##"<< endl;
	cout << "    0   1   2 " << endl;
	cout << "   ___ ___ ___" << endl;
	
	for (int r = 0; r < size; r++) {
		cout << r << " |";
		for (int c = 0; c < size; c++) {
			cout << " ";
			if (board[r][c] != 0) 
				cout << board[r][c];
			else 
				cout << " ";
			cout << " |";
		}
	cout << "\n  |___|___|___|" << endl;
	}
}	


int Board::getSize()
{
	return size;
}


int Board::getWinSum()
{
	return winSum;
}
