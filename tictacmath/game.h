//Maria Lobanova
//Tic Tac Math

#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <vector>
#include <iomanip>
#include "Player.h"
#include "Board.h"

using namespace std;

class Game
{
	private:	int currentPlayerIdx;
						int turnsLeft;
						vector<int> playedNums;
						vector<Player> players;
						Board board;

	public:		Game();
						void printGameStatistics();
						void playMove(int,int,int);
						void displayBoard();
						void switchPlayer();
						bool addPlayedNum(int);
						bool addPlayer(Player); 
						bool isMovePossible(int,int);
						bool isNumValid(int);
						short int evaluateGameState();
						int getTurnsLeft();
						int getWinSum(); //delegate to board
						Player getCurrentPlayer();
};		

#endif
