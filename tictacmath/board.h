//Maria Lobanova
//Tic Tac Math

#ifndef BOARD_H
#define BOARD_H
#include <iostream>
#include <string>

using namespace std;

class Board
{ 
	private:	
						int	**board;
						int size;
						int winSum;
	public:		
						Board();
						Board(int, int);
						void display();
						void setValueToBoard(int, int, int);
						bool isPositionAvailable(int, int);
						bool isPositionValid(int, int);
						bool isNumValid(int);
						int rowSum(int);
						int columnSum(int);
						int lDiagonalSum();
						int rDiagonalSum();
						int getSize();
						int getWinSum();
};

#endif
