//Maria Lobanova
//Tic Tac Math

#ifndef INTERFACE_H
#define INTERFACE_H

#include <iostream>
#include "game.h"
#include "player.h"
#include "validator.h"

using namespace std;

//class is reponsible for all interactions with the user
class Interface
{	
	private: Game game;
					 void setPlayer();

	public:	Interface();
					void startNewGame();
					void displayGameRules();
					int getPlayNum();
					pair<int,int> getPosition(); //get row and column
};

#endif
