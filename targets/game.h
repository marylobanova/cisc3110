//Maria Lobanova
//Targets

#ifndef GAME_H
#define GAME_H
#include "Player.h"
#include <vector>

class Game
{
	private: 
						int turnsLeft;
						int currentPlayerIdx;
						vector <Player> players;
						Player player;
	public:
						Game();
						void switchPlayer();
						void displayOpponentBoard();
						bool addPlayer(Player);
						bool playMove(int);
						bool isMovePossible(int);
						bool isOver();
						int getTurnsLeft();
						Player & getCurrentPlayer();
						Player & getOpponent();
};

#endif
