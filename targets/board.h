//Maria Lobanova
//Targets

#ifndef BOARD_H
#define BOARD_H

#include "Target.h"
#include <algorithm>
#include <vector>
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

class Board
{
	private:
						int size;
						int numTargetsPerSize;
						int numTargetSizes;					//number of different target sizes
						bool isEnoughSpace(); 			//board size should be greater
																				//than size of all targets
						vector <int> targetSizes;		//stores sizes of targets
						vector <Target> targets;		//stores targets
						vector <char> board; 				// X - hit, O - miss
						void generateTargets();
						void assignTargetsToBoard(); 
	public: 
						Board();
						Board(Board& b);
						void display();
						bool isPositionValid(int);
						bool isTargetHit(int);
						bool recordMove(int); 
						bool isMoveAlreadyMade(int);
						bool areAllTargetsHit();
						int getSize();
						int getTotalTargetsSize();
};

#endif
