//Maria Lobanova
//Targets

#include "Target.h"

//default constructor
Target::Target()
{
	size = 2;
	idx = 0;
	condition = size;
}


//constructor with dynamic size
Target::Target(int size)
{
	this->size = size;
	idx = 0;
	condition = size;
}


//constructor with dynamic size and position
Target::Target(int size, int idx)
{
	this->size = size;
	this->idx = idx;
	this->condition = size;
}


int Target::getSize()
{
	return size;
}


void Target::setPosition(int idx)
{
	this->idx = idx;
}


int Target::getPosition()
{
	return idx;
}


int Target::getCondition()
{
	return condition;
}


void Target::updateCondition()
{
	if (condition > 0)
		condition--;
}
