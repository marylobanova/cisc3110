//Maria Lobanova
//Targets

#include "Interface.h"

//default constructor
Interface::Interface(){}

void Interface::startNewGame()
{	
	Player player;
	int move;
	string response;

	//ask if user wants to start another game
	//if the previous game is over
	do {
		cout << "Do you want to start a new game? (y/n) ";
		getline (cin,response);

		//validate for correct response
		while (response != "y" &&  response != "Y" && response != "n" && response != "N") {
			cout << "This is invalid answer. Please enter y (for yes) or n (for no): ";
			getline (cin, response);
			}

		//start new game
		if (response == "y" || response == "Y") {
			displayRules();
			createPlayers();

			//continue the game until there are no turns left
			//of if there is a winner
			while (game.getTurnsLeft() >= 0) {
				player = game.getCurrentPlayer();
				cout << player.getName() << ", please make your move." << endl;
				game.displayOpponentBoard();
				move = getMove();
				
				if (game.playMove(move)) 
					cout << "### IT WAS A HIT! ###" << endl;
				else
					cout << "You missed." << endl;
				cout << "--------------------------------------" << endl;
				
				//exit current game if player won
				if (game.isOver()) {
					cout << "\nCONGRATULATION! You won!" << endl;
					break;
				}
				cout << "BOARD OF: " << player.getName() << endl;
			}

		}
		//exit the game if user has chosen not to continue
		else {
			cout << "\nGoodbye!\n" << endl;
			exit(0);
		}
	} while (response == "y" || response == "Y");

	exit(0);

}


void Interface::createPlayers()
{
	string name;
	for (int i = 0; i < 2; i++) {
		cout << "Enter " << i+1 << " player's name: ";
		getline (cin, name);
		//player name has to be unique
		Player player;
		player.setName(name);
		while (!game.addPlayer(player)) {
			cout << "This name has already been taken. Choose different name: ";
			getline (cin, name);
			player = Player();
			player.setName(name);
		}
	}
	cout << "--------------------------------------" << endl;
}


int Interface::getMove()
{
	int move;
	string sMove;
	//keep asking to choose a number
	//if the number is not valid
	do {
		cout << "Choose a hit position: ";
		cin >> sMove;
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //ignore new line char
		//check if an input is a digit
		while (!isNumber(sMove)) {
			cout << "This entry is invalid. Please enter a number: ";
			cin >> sMove;
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //ignore new line char
		}
		move = stoi(sMove, nullptr, 10); //convert an input to an integer

		if (!game.isMovePossible(move-1)){
			cout << "Move is not possible. Either this number is not in the range of "; 
			cout << "available numbers or such move was already played. Try again." << endl;
		}
	} while (!game.isMovePossible(move-1));

	return move-1;
}


void Interface::displayRules()
{
	cout << endl;
	cout << "### RULES OF THE GAME ###" << endl;
	cout << "Hello! This game is played by two persons. Each person has a board" << endl;
	cout << "and each board has 5 targets on it. The sizes of targets are 2,3,4,5,6" << endl;
	cout << "To make a move choose a position which you want to play. If you hit" << endl;
	cout << "the target you will see 'X' on that position. If you missed the target" << endl;
	cout << "you will see 'O' on that position." << endl;
	cout << "First player who hit all opponent's targets will win the game." << endl;
	cout << "Good luck!" << endl;
	cout << endl;
}

