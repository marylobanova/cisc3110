//Maria Lobanova
//Targets

#include "board.h"

Board::Board()
{
	size = 30;
	numTargetsPerSize = 1;
	numTargetSizes = 5; 
	board.assign(size, ' '); //assign all values to be empty
	//assign default target sizes to 2,3,4,5,6
	for (int i = 0; i < numTargetSizes; i++ )
		targetSizes.push_back(i+2);
	generateTargets();
	assignTargetsToBoard();
}


Board::Board(Board & b)
{
  board = b.board;
	targetSizes = b.targetSizes;
	numTargetSizes = b.numTargetSizes;
  size = b.size;
  numTargetsPerSize = b.numTargetsPerSize;
}


bool Board::areAllTargetsHit()
{
	for (int i = 0; i < targets.size(); i++) 
		if( targets[i].getCondition() > 0 ) 
			return false;
	return true;
}


int Board::getSize()
{
	return size;
}


//checks if user's move falls onto any of cells 
//occupied by each target
bool Board::isTargetHit (int idx)
{
	int pos; 
	for (int i = 0; i < targets.size(); i++) {
		pos = targets[i].getPosition();
		if(idx >= pos && idx < pos + targets[i].getSize()) {
			targets[i].updateCondition();		//decrement health
			return true;
		}
	}
	return false;
}


void Board::display()
{
	for (int i = 0; i < size; i++) 
		cout << " " << setw(2) << i+1 << " ";
	cout << endl;
	for (int i = 0; i < size; i++) 
		cout << " ___";
	cout << endl;
	for (int i = 0; i < size; i++)
		cout << "| " << board[i] << " ";
	cout << "|" << endl;
	for (int i = 0; i < size; i++) 
		cout << "|___";
	cout << "|" <<  endl;
}


void Board::assignTargetsToBoard()
{
	int position = 0; 												//position on the board
	int targetIdx = targets.size()-1;					//position of each target
																						//initialized to the last element
	int targetsSize = getTotalTargetsSize();	//total size of all targets
	
	//shuffle targets to be in the random order
	random_shuffle ( targets.begin(), targets.end() );
	
	//continue while there is a space on the board
	//and there are remaining targets
	while (position < size && targetIdx >= 0) {
		//generate random number for space size number
		//should be at least zero and at most a difference
		//between board size and total target sizes 
		//combined with one additional space for each target
		position += rand() % (size-targetsSize-position-targetIdx) + 1;
		targets[targetIdx].setPosition(position);
		position += targets[targetIdx].getSize();
		targetsSize-=targets[targetIdx].getSize();
		targetIdx--;
	}
}


int Board::getTotalTargetsSize()
{
	int totalTargetsSize = 0;
	for (int i = 0; i < numTargetSizes; i++)
		totalTargetsSize += targetSizes[i]*numTargetsPerSize;
	return totalTargetsSize;
}


bool Board::isEnoughSpace()
{
	//add spacing between targets to the total size of targets
	return (size > (getTotalTargetsSize() + numTargetSizes));
}


bool Board::isPositionValid(int position)
{
	return (0 <= position && position < size);
}


//X - hit, O - miss
bool Board::recordMove(int move)
{
	bool ret = isTargetHit(move);
	board[move] = ret ? 'X' : 'O';
	return ret;
}


bool Board::isMoveAlreadyMade(int move)
{
	return board[move] != ' ';
}


void Board::generateTargets()
{
	//generate tartgets only if there is enough space
	if (isEnoughSpace()){
		for (int i = 0; i < numTargetsPerSize; i++){
			for (int n = 0; n < numTargetSizes; n++){
				targets.push_back(Target(targetSizes[n]));
			}
		}
	}
} 
