//Maria Lobanova
//Targets

#ifndef TARGET_H
#define TARGET_H

class Target
{
	private:
						int size;
						int idx;				//location
						int condition; 	//0 - dead
	public:
						Target();
						Target(int);
						Target(int,int);
						void setPosition(int);
						void updateCondition(); //condition is decremented for each hit 
						int getSize();
						int getPosition();
						int getCondition();
};


#endif
