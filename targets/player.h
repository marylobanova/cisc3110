//Maria Lobanova
//Targets

#ifndef PLAYER_H
#define PLAYER_H
#include <string>
#include "Board.h"

using namespace std;

class Player
{
	private: 
					string name;
					 Board board;
					
	public: 
					Player();
					Player(const Player&);
					void setName(string);
					string getName();
					Board & getBoard();
};

#endif
