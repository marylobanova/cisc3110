//Maria Lobanova
//Targets

#ifndef INTERFACE_H
#define INTERFACE_H

#include <iostream>
#include "Board.h"
#include "Player.h"
#include "Validator.h"
#include "Game.h"

using namespace std;

class Interface
{
	private:
						Game game;
	public:
						Interface();
						void startNewGame();
						void displayRules();
						void createPlayers();
						int getMove();

};

#endif
