//Maria Lobanova
//Targets

#include "Game.h"

//store all players

Game::Game()
{
	turnsLeft = player.getBoard().getSize()*2;	//board size times number of players
	currentPlayerIdx = 0;												//set current player to first added
}


bool Game::addPlayer(Player player)
{
	for (int i=0; i < players.size(); i++){
		if (player.getName() == players[i].getName())
			return false;
	}
	players.push_back(player);
	return true;
}


bool Game::playMove(int move)
{
	bool ret = getOpponent().getBoard().recordMove(move);
	turnsLeft--;
	if (!isOver())
		switchPlayer(); //switch player if there is no winer
	return ret;
}


bool Game::isOver()
{
	return getOpponent().getBoard().areAllTargetsHit();
}


bool Game::isMovePossible(int move)
{
	return (!getOpponent().getBoard().isMoveAlreadyMade(move) && 
					getOpponent().getBoard().isPositionValid(move));
}


void Game::displayOpponentBoard()
{
	getOpponent().getBoard().display();
}


Player & Game::getCurrentPlayer()
{
	return players[currentPlayerIdx];
}


Player & Game::getOpponent()
{
	int opponent = (currentPlayerIdx+1)%players.size();
	return players[opponent];
}


void Game::switchPlayer()
{
	currentPlayerIdx = (currentPlayerIdx+1)%players.size();
}


int Game::getTurnsLeft()
{
	return turnsLeft;
}
