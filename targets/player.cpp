//Maria Lobanova
//Targets

#include "Player.h"

//default constructor
Player::Player()
{
	name = "Anomymous";
}


//copy constructor
Player::Player(const Player & p) : name(p.name) { }


void Player::setName(string n) 
{
	name = n;
}


string Player::getName()
{
	return name;
}


Board & Player::getBoard()
{
	return board;
}
